import controller.Controller;
import controller.IController;
import model.Device;
import model.Led;

/**
 * Created by Utente on 05/12/2017.
 */
public class Main{
    public static void main(String[] args) {
        Device led=new Led ();
        IController controller=new Controller (led);


        System.out.println(controller.isDeviceOnList (led));
    }
}
