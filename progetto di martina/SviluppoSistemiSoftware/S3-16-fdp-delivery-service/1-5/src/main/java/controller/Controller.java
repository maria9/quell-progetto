package controller;

import model.Device;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Utente on 05/12/2017.
 */
public class Controller implements IController{
private Device device;
private List<Device> list=new ArrayList <> ();

    public Controller(Device device){
        this.device=device;
    }

    @Override
    public void addDevice(Device device) {
    this.list.add (device);
    }

    @Override
    public void removeDevice(Device device) {
   this.list.remove (device);
    }

    @Override
    public boolean isDeviceOnList(Device device) {
        return this.device.getState ();
    }
}
