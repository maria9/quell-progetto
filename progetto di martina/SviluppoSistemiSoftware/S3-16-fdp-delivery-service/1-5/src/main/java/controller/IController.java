package controller;

import model.Device;

/**
 * Created by Utente on 05/12/2017.
 */
public interface IController{

    void addDevice(Device device);
    void removeDevice(Device device);
    boolean isDeviceOnList(Device device);

}
