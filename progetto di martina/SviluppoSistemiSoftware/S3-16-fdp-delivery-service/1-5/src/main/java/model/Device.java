package model;

/**
 * Created by Utente on 05/12/2017.
 */
public interface Device{
    void setOn();
    void setOff();
    boolean getState();
}
