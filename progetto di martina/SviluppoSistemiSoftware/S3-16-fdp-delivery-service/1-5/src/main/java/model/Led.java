package model;

/**
 * Created by Utente on 05/12/2017.
 */
public class Led implements Device{
private boolean isDeviceOn;

public Led(){
    this.isDeviceOn=isDeviceOn;
}

    @Override
    public void setOn() {
        this.isDeviceOn=true;
    }

    @Override
    public void setOff() {
        this.isDeviceOn=false;
    }

    @Override
    public boolean getState() {
        return this.isDeviceOn;
    }
}
