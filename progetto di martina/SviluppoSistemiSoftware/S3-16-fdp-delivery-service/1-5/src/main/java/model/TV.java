package model;

/**
 * Created by Utente on 05/12/2017.
 */
public class TV implements Device{

    private int x=0;
    @Override
    public void setOn() {
       this.x=1;
    }

    @Override
    public void setOff() {
    this.x=0;
    }

    @Override
    public boolean getState() {
       if (x==0){
        return false;
    }else{
        return true;}
}}

