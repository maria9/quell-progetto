package Controller;

import View.PanelView;

/**
 * Created by Martina on 22/07/2017.
 */

public class AppController
{
    private static PanelView theMainPanelView = null;


    public static void BuildMainPanelView()
    {
       if (theMainPanelView == null) {
          theMainPanelView = new PanelView();
          theMainPanelView.showPanel();
       }
    }

    public static void ShowMainPanelView()
    {
       theMainPanelView.setVisible (true);
    }

   public static void HideMainPanelView()
   {
      theMainPanelView.setVisible (false);
   }

}