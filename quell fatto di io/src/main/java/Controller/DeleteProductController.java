package Controller;

import View.DeleteProductView;

/**
 * Created by Martina on 27/07/2017.
 */

public class DeleteProductController {

    private static DeleteProductView theDelProdView;



    public static void BuildDelProdView()
    {
        if (theDelProdView == null) {
            theDelProdView = new DeleteProductView();
            theDelProdView.showPanel();
        }
    }

    public static void ShowDelProdView()
    {
        theDelProdView.setVisible (true);
    }

    public static void HideDelProdView()
    {
        theDelProdView.setVisible (false);
    }
}
