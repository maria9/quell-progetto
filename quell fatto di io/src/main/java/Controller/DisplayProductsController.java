package Controller;

import View.DisplayProductsView;

/**
 * Created by Martina on 27/07/2017.
 */

public class DisplayProductsController {

    private static DisplayProductsView theDisplayProdsView = null;

    public static void BuildDisplayProdsView()
    {
        if (theDisplayProdsView == null) {
            theDisplayProdsView = new DisplayProductsView();
            theDisplayProdsView.showPanel();
        }
    }

    public static void ShowDisplayProdsView()
    {
        theDisplayProdsView.setVisible (true);
    }

    public static void HideDisplayProdsView()
    {
        theDisplayProdsView.setVisible (false);
    }



}
