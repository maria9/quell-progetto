package Controller;

import View.InsertProductView;

/**
 * Created by Martina on 27/07/2017.
 */

public class InsertProductController {

    private static InsertProductView theInsProdView = null;



    public static void BuildInsProdView()
    {
        if (theInsProdView == null) {
            theInsProdView = new InsertProductView();
            theInsProdView.showPanel();
        }
    }

    public static void ShowInsProdView()
    {
        theInsProdView.setVisible (true);
    }

    public static void HideInsProdView()
    {
        theInsProdView.setVisible (false);
    }

}
