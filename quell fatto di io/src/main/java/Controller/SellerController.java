package Controller;

import View.SellerView;

/**
 * Created by Martina on 22/07/2017.
 */

public class SellerController {

    private static SellerView theSellerView = null;

    public static void BuildSellerView()
    {
        if (theSellerView == null) {
            theSellerView = new SellerView();
            theSellerView.showPanel();
        }
    }

    public static void ShowSellerView()
    {
        theSellerView.setVisible (true);
    }

    public static void HideSellerView()
    {
        theSellerView.setVisible (false);
    }


}