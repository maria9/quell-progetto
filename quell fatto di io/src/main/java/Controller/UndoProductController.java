package Controller;
import View.UndoProductView;

/**
 * Created by Utente on 30/07/2017.
 */
public class UndoProductController{

    private static UndoProductView scene;

    public UndoProductController() {
        this.scene = new UndoProductView();
    }

    public void showPlatform() {
        this.scene.showPanel();
    }

}