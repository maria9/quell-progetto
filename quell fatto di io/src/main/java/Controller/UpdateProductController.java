package Controller;

import View.UpdateProductView;

/**
 * Created by Martina on 27/07/2017.
 */

public class UpdateProductController {

    private static UpdateProductView theUpdateProdView = null;

    public static void BuildUpdateProdView()
    {
        if (theUpdateProdView == null) {
            theUpdateProdView = new UpdateProductView();
            theUpdateProdView.showPanel();
        }
    }

    public static void ShowUpdateProdView()
    {
        theUpdateProdView.setVisible (true);
    }

    public static void HideUpdateProdView()
    {
        theUpdateProdView.setVisible (false);
    }


}
