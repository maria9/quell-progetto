package View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created by Utente on 28/07/2017.
 */
public class BuyerView extends JFrame{

    public void showPanel() {

        JLabel l;
        JTextField t;
        JButton b;
        JFrame f = new JFrame("FDP - Fast Delivery Pizza");
        Container cp = f.getContentPane();
        cp.setLayout(new GridBagLayout());
        cp.setBackground(UIManager.getColor("control"));
        GridBagConstraints c = new GridBagConstraints();

        c.gridx = 0;
        c.gridy = GridBagConstraints.RELATIVE;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.insets = new Insets(2, 2, 2, 2);
        c.anchor = GridBagConstraints.EAST;


        cp.add(l = new JLabel("Product:", SwingConstants.RIGHT), c);
        l.setDisplayedMnemonic('n');


        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 1.0;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.CENTER;

        cp.add(t = new JTextField(35), c);
        t.setFocusAccelerator('n');
        c.gridx = 1;
        c.gridy = GridBagConstraints.RELATIVE;
        c.weightx = 0.0;
        c.fill = GridBagConstraints.NONE;


        f.pack();
        f.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent evt) {
                System.exit(0);
            }
        });
        f.setVisible(true);
    }
}
