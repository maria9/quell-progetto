package View;

import Controller.AppController;
import Controller.DeleteProductController;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
/**
 * Created by Martina on 27/07/2017.
 */

public class DeleteProductView extends JFrame
{

    public void showPanel() {
        setTitle( "FDP - Fast Delivery Pizza" );
        setSize( 289, 243 );
        setVisible( true );

        JLabel label = new JLabel("<html>Some Text</html>");
        add(label);

        
        JButton deleteButton = new JButton( "Delete" );
        deleteButton.setBounds( 105, 10, 110, 35 );


        JButton undoButton = new JButton( "Back" );
        undoButton.setBounds( 3, 10, 75, 35 );

        undoButton.addActionListener((ActionEvent arg0) -> {
            Thread queryThread = new Thread() {
                public void run() {
                    DeleteProductController.HideDelProdView();
                    SellerView.ShowSellerView();

                }
            };
            queryThread.start();
        });

        final ActionListener actionListener = (ActionEvent arg0) -> {
            Thread queryThread = new Thread(){
                public void run() {

                    AppController.HideMainPanelView();
                    DeleteProductController.BuildDelProdView();
                    DeleteProductController.ShowDelProdView();

                }
            };
            queryThread.start();
        };
        deleteButton.addActionListener( actionListener );


        JPanel panel = new JPanel();
        panel.setLayout(null);
        panel.add(deleteButton);
        panel.add(undoButton);

        String[] theColNames = {"name", "ingreds", "price"};
        Object[][] theTableData = {
           {"pizza", "tomato, cheese", 3},
           {"pizza ext", "tomato, cheese", 5}
        };

        JTable theDataTable = new JTable (theTableData, theColNames);

        theDataTable.setFillsViewportHeight (true);

        JScrollPane theListPanel = new JScrollPane (theDataTable);

        theListPanel.setBounds (30, 48, 260, 260);

        //theListPanel.add (theDataTable);

        panel.add (theListPanel);

        this.getContentPane().add(panel);

        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

    }
}




