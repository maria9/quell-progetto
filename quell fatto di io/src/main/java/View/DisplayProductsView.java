package View;


import Controller.DisplayProductsController;
import Controller.SellerController;

import javax.swing.*;
import java.awt.event.ActionEvent;


/**
 * Created by Martina on 27/07/2017.
 */

public class DisplayProductsView extends JFrame{

    public void showPanel() {
        setTitle( "FDP - Fast Delivery Pizza" );
        setSize( 289, 243 );
        setVisible( true );



        JButton undoButton = new JButton( "Back" );
        undoButton.setBounds( 3, 8, 75, 35 );

        undoButton.addActionListener((ActionEvent arg0) -> {
            Thread queryThread = new Thread() {
                public void run() {

                    DisplayProductsController.HideDisplayProdsView();
                    SellerController.ShowSellerView();


                }
            };
            queryThread.start();
        });


        JPanel panel = new JPanel();
        panel.setLayout(null);
        panel.add(undoButton);

        String[] theColNames = {"name", "ingreds", "price"};
        Object[][] theTableData = {
           {"pizza", "tomato, cheese", 3},
           {"pizza ext", "tomato, cheese", 5}
        };

        JTable theDataTable = new JTable (theTableData, theColNames);

        theDataTable.setFillsViewportHeight (true);

        JScrollPane theListPanel = new JScrollPane (theDataTable);

        theListPanel.setBounds (30, 48, 260, 260);

        //theListPanel.add (theDataTable);

        panel.add (theListPanel);

        this.getContentPane().add(panel);

        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

    }
}















