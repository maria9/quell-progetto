package View;

/**
 * Created by Martina on 22/07/2017.
 */

public class Main
{
    public static void main(String[] args){

        Controller.AppController.BuildMainPanelView();
        Controller.AppController.ShowMainPanelView();
    }
}