package View;

import Controller.SellerController;

import javax.swing.*;

import java.awt.event.ActionEvent;

/**
 * Created by Martina on 26/07/2017.
 */

public class Panel extends JFrame {
    public void showPanel(){
        setTitle("FDP - Fast Delivery Pizza");
        setSize(289, 243);

        //Create JButton and JPanel
        JButton sellerButton = new JButton("Seller");
        sellerButton.setBounds(3, 54, 75, 35);
        sellerButton.addActionListener((ActionEvent arg0) -> {
            Thread queryThread = new Thread() {
                public void run() {
                    //Set invisible precedent Panel
                    setVisible(false);

                    //Call Seller controller
                    SellerController sellerController = new SellerController();

                    //Show Platform Seller
                    sellerController.showPlatform();
                }
            };
            queryThread.start();
        });

        JButton buyer = new JButton("Buyer");
        buyer.setBounds(160, 45, 75, 35);
        buyer.addActionListener((ActionEvent arg0) -> {
            Thread queryThread = new Thread() {
                public void run() {
                    System.out.print("Buyer!");
                    //TODO : Call controller Buyer
                }
            };
            queryThread.start();
        });

        JPanel panel = new JPanel();
        panel.setLayout(null);

        //Add button to JPanel
        panel.add(sellerButton);
        panel.add(buyer);
        // And JPanel needs to be added to the JFrame itself!
        this.getContentPane().add(panel);

        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}