package View;

import Controller.AppController;
import Controller.BuyerController;
import Controller.SellerController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * Created by Martina on 22/07/2017.
 */

public class PanelView extends JFrame {
    public void showPanel(){
        setTitle("FDP - Fast Delivery Pizza");
        setSize(289, 243);

        //Create JButton and JPanel
        JButton sellerButton = new JButton("Seller");
        sellerButton.setBounds(3, 54, 75, 35);
        sellerButton.addActionListener((ActionEvent arg0) -> {
            Thread queryThread = new Thread() {
                public void run() {
                    AppController.HideMainPanelView();
                    SellerController.BuildSellerView();
                    SellerController.ShowSellerView();

                }
            };
            queryThread.start();
        });

        JButton buyer = new JButton("Buyer");
        buyer.setBounds(160, 45, 75, 35);
        buyer.addActionListener((ActionEvent arg0) -> {
            Thread queryThread = new Thread() {
                public void run() {
                    //Set invisible precedent PanelView
                    setVisible(false);

                    //Call SellerView controller
                    BuyerController buyerController = new BuyerController();

                    //Show Platform SellerView
                    buyerController.showPlatform();
                }

                };

            queryThread.start();
        });

        Container cp = getContentPane();
        cp.setLayout(new FlowLayout());
        cp.add(sellerButton);
        cp.add(buyer);





        JPanel panel = new JPanel();
        panel.setLayout(null);

      ;
        // And JPanel needs to be added to the JFrame itself!
        this.getContentPane().add(panel);

        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}