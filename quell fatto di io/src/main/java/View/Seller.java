package View;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Martina on 26/07/2017.
 */

public class Seller extends JFrame {
    public void showPanel(){
        setTitle("FDP - Fast Delivery Pizza");
        setSize(289, 280);
        setVisible(true);

        JButton displayButton = new JButton("Display");
        displayButton.setBounds(3, 6, 75, 35);

        JButton insertButton = new JButton("Insert");
        insertButton.setBounds(100, 10, 75, 35);

        ImageIcon icon = new ImageIcon("java-swing-tutorial.JPG");
        JTabbedPane jtbExample = new JTabbedPane();
        JPanel jplInnerPanelProducts = createInnerPanel("");

        jplInnerPanelProducts.add(displayButton);
        jplInnerPanelProducts.add(insertButton);

        jtbExample.addTab("Products", icon, jplInnerPanelProducts, "Tab 1");
        jtbExample.setSelectedIndex(0);

        JPanel jplInnerPanel2 = createInnerPanel("Tab 2 Contains Icon only");
        jtbExample.addTab("Orders", icon, jplInnerPanel2);

        //Add the tabbed pane to this panel.
        setLayout(new GridLayout(1, 1));
        add(jtbExample);
    }

    protected JPanel createInnerPanel(String text) {
        JPanel jplPanel = new JPanel();
        JLabel jlbDisplay = new JLabel(text);
        jlbDisplay.setHorizontalAlignment(JLabel.CENTER);
        jplPanel.setLayout(new GridLayout(1, 1));
        jplPanel.setLayout(null);
        jplPanel.add(jlbDisplay);
        return jplPanel;
    }
}