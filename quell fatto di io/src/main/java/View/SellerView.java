package View;

import Controller.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * Created by Martina on 22/07/2017.
 */

public class SellerView extends JFrame
{
   private static SellerView theSellerView = null;

   public static void ShowSellerView()
   {
      theSellerView.setVisible (true);
   }

   public static void HideSellerView()
   {
      theSellerView.setVisible (false);
   }

    public void showPanel()
    {
       theSellerView = this;

        setTitle("FDP - Fast Delivery Pizza");
        setSize(289, 280);
        setVisible(true);

        //Define button
        JButton displayButton = new JButton("Display");
        displayButton.setBounds(100, 6, 110, 35);

        JButton insertButton = new JButton("Insert");
        insertButton.setBounds(100, 46, 110, 35);

        JButton updateButton = new JButton("Update");
        updateButton.setBounds(100, 86, 110, 35);

        JButton deleteButton = new JButton("Delete");
        deleteButton.setBounds(100, 126, 110, 35);

        JButton undoButton = new JButton("Undo");
        undoButton.setBounds(3, 34, 75, 35);





        ImageIcon icon = new ImageIcon("java-swing-tutorial.JPG");
        JTabbedPane jtbExample = new JTabbedPane();
        JPanel jplInnerPanelProducts = createInnerPanel(" " );

        //Add button to PanelView
        jplInnerPanelProducts.add(displayButton);
        jplInnerPanelProducts.add(insertButton);
        jplInnerPanelProducts.add(updateButton);
        jplInnerPanelProducts.add(deleteButton);
        jplInnerPanelProducts.add(undoButton);

        /*Click button*/

        //click for display button
        displayButton.addActionListener((ActionEvent arg0) -> {
            Thread queryThread = new Thread() {
                public void run() {
                    //Set invisible precedent PanelView
                    //setVisible(false);
                   HideSellerView();
                   DisplayProductsController.BuildDisplayProdsView();
                   DisplayProductsController.ShowDisplayProdsView();
                    //Call Display Products Controller
                    //DisplayProductsController displayProductsController = new DisplayProductsController();

                    //Show Display Products View
                    //displayProductsController.showPlatform();
                }
            };
            queryThread.start();
        });

        //click for insert button
        insertButton.addActionListener((ActionEvent arg0) -> {
            Thread queryThread = new Thread() {
                public void run() {

                   HideSellerView();

                   InsertProductController.BuildInsProdView();
                   InsertProductController.ShowInsProdView();


                }
            };
            queryThread.start();
        });

        //click for update button
        updateButton.addActionListener((ActionEvent arg0) -> {
            Thread queryThread = new Thread() {
                public void run() {

                   HideSellerView();
                   UpdateProductController.BuildUpdateProdView();
                   UpdateProductController.ShowUpdateProdView();


                }
            };
            queryThread.start();
        });

        //click for delete button
        deleteButton.addActionListener((ActionEvent arg0) -> {
            Thread queryThread = new Thread() {
                public void run() {

                   HideSellerView();
                   DeleteProductController.BuildDelProdView();
                   DeleteProductController.ShowDelProdView();

                }
            };
            queryThread.start();
        });

        //click for delete button
        undoButton.addActionListener((ActionEvent arg0) -> {
            Thread queryThread = new Thread() {
                public void run() {

                   HideSellerView();


                   AppController.ShowMainPanelView();
                }
            };
            queryThread.start();
        });




        jtbExample.addTab("Products", icon, jplInnerPanelProducts, "Tab 1");
        jtbExample.setSelectedIndex(0);

        JPanel jplInnerPanel2 = createInnerPanel("Tab 2 Contains Icon only");
        jtbExample.addTab("Orders", icon, jplInnerPanel2);

        //Add the tabbed pane to this panel.
        setLayout(new GridLayout(1, 1));
        add(jtbExample);
        this.pack();
    }

    protected JPanel createInnerPanel(String text) {
        JPanel jplPanel = new JPanel();
        JLabel jlbDisplay = new JLabel(text);
        jlbDisplay.setHorizontalAlignment(JLabel.CENTER);
        jplPanel.setLayout(new GridLayout(1, 1));
        jplPanel.setLayout(null);
        jplPanel.add(jlbDisplay);
        return jplPanel;
    }



        }
