package View;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Utente on 30/07/2017.
 */
public class UndoProductView extends JFrame{
    public void showPanel() {
        setTitle( "FDP - Fast Delivery Pizza" );
        setSize( 289, 280 );
        setVisible( true );


    }
    public void exchangeComponents(int component1,int component2,JPanel jpanel){
        try{
            Component aux1 = jpanel.getComponent(component1);
            Point aux1Loc = aux1.getLocation();
            Component aux2 = jpanel.getComponent(component2);
            Point aux2Loc = aux2.getLocation();
            aux1.setLocation(aux2Loc);
            aux2.setLocation(aux1Loc);
        }
        catch (ArrayIndexOutOfBoundsException ex){ /* error! bad input to the function*/
            System.exit(1);
        }
    }
}