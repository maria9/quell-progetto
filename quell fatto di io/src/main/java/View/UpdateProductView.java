package View;

import Controller.SellerController;
import Controller.UpdateProductController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created by Martina on 27/07/2017.
 */

public class UpdateProductView extends JFrame
{
    public void showPanel()
    {
        JLabel l;
        JTextField t;
        JButton b;

        JFrame f = this;
        f.setTitle ("FDP - Fast Delivery Pizza ");
        Container cp = f.getContentPane();
        cp.setLayout(new GridBagLayout());
        cp.setBackground(UIManager.getColor("control"));
        GridBagConstraints c = new GridBagConstraints();

        c.gridx = 0;
        c.gridy = GridBagConstraints.RELATIVE;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.insets = new Insets(2, 2, 2, 2);
        c.anchor = GridBagConstraints.EAST;

        cp.add(l = new JLabel("Product:", SwingConstants.RIGHT), c);
        l.setDisplayedMnemonic('n');
        cp.add(l = new JLabel("Ingredients:", SwingConstants.RIGHT), c);
        l.setDisplayedMnemonic('h');
        cp.add(l = new JLabel("Price:", SwingConstants.RIGHT), c);
        l.setDisplayedMnemonic('c');

        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 1.0;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.CENTER;

        cp.add(t = new JTextField(35), c);
        t.setFocusAccelerator('n');
        c.gridx = 1;
        c.gridy = GridBagConstraints.RELATIVE;
        cp.add(t = new JTextField(35), c);
        t.setFocusAccelerator('h');
        cp.add(t = new JTextField(35), c);
        t.setFocusAccelerator('c');
        c.weightx = 0.0;
        c.fill = GridBagConstraints.NONE;
        cp.add(b = new JButton("Update"), c);
        b.setMnemonic('o');

        c.fill = GridBagConstraints.NONE;

        b.setMnemonic('o');

        JButton theGoBackBtn = new JButton ("Back");
        cp.add (theGoBackBtn);
        theGoBackBtn.setMnemonic('o');
        theGoBackBtn.addActionListener (
                (ActionEvent arg0) -> {

                    UpdateProductController.HideUpdateProdView();
                    SellerView.ShowSellerView();
                }
        );





        f.pack();
        f.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent evt) {
                System.exit(0);
            }
        });
        f.setVisible(true);



        JButton undoButton = new JButton( "Back" );
        undoButton.setBounds( 3, 34, 75, 35 );


        undoButton.addActionListener((ActionEvent arg0) ->  {

            Thread queryThread = new Thread() {
                public void run() {


                    UpdateProductController.HideUpdateProdView();

                    SellerController.ShowSellerView();


                }
            };
            queryThread.start();
        });




    }







}










